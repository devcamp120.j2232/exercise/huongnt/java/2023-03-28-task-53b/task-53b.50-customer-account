import javax.security.auth.login.AccountLockedException;

public class Account {
    int id;
    Customer customer;
    double balance = 0.0;

    // phương thức khởi tạo có tham số
    public Account(int id, Customer customer, double balance) {
        this.id = id;
        this.customer = customer;
        this.balance = balance;
    }

    // getter
    public int getId() {
        return id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public double getBalance() {
        return balance;
    }

    // setter balance
    public void setBalance(double balance) {
        this.balance = balance;
    }

    // in ra console
    @Override
    public String toString() {
        // balance làm tròn đến 2 chữ số thập phân
        double roundOff = (double) Math.round(balance * 100) / 100;
        return String.format("(Name = %s) (id =%s) (balance = $%s)", customer.name, id, roundOff);
    }

    // getter customer name
    public String getCustomerName() {
        return customer.getName();
    }

    /**
     * 
     * @param amount
     * @return
     */
    public Account deposit(double amount) {
        this.balance += amount;
        return this;
    }

    /**
     * 
     * @param amount
     * @return
     */
    public Account withdraw (double amount){
        if (this.balance >=amount) {
            this.balance -= amount;
        }
        else {System.out.println("Amount withdrawn exceeds the current balance!")}
        return this;
    }

}
