public class App {
    public static void main(String[] args) throws Exception {
        // khởi tạo đối tượng cuastomer 1
        Customer customer1 = new Customer(1, "Lan", 20);
        // khởi tạo đối tượng cuastomer 2
        Customer customer2 = new Customer(2, "Hoang", 30);

        // in ra console
        System.out.println(customer1.toString());
        System.out.println(customer2.toString());

        // khởi tạo đối tượng account 1
        Account account1 = new Account(1, customer1, 4500.0);
        Account account2 = new Account(2, customer2, 8500.0);

        // im hai đối tượng account ra console
        System.out.println(account1.toString());
        System.out.println(account2.toString());
    }
}
