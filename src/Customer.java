public class Customer {
    int id;
    String name;

    //Discount in percent
    int discount;

        //phương thức khởi tạo có tham số
    public Customer(int id, String name, int discount) {
        this.id = id;
        this.name = name;
        this.discount = discount;
    }

        //getter
    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getDiscount() {
        return discount;
    }

    //in ra console
    @Override
    public String toString(){
        return String.format("(Name = %s) (id =%s)(discount = %s%%)", name, id, discount);
    }

    
    
    
}
